package xyz.mnsf.dept_assignment.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import xyz.mnsf.dept_assignment.models.Movie;
import xyz.mnsf.dept_assignment.models.Series;
import xyz.mnsf.dept_assignment.models.Video;
import xyz.mnsf.dept_assignment.models.VideoResults;

import java.net.URI;
import java.util.List;
import java.util.Locale;

@Repository
public class TheMovieDatabase implements MovieRepo, SeriesRepo {
  private static Logger logger = LoggerFactory.getLogger(TheMovieDatabase.class);
  private static final String BASE_URL_MOVIE = "https://api.themoviedb.org/3/movie";
  private static final String BASE_URL_SEARCH = "https://api.themoviedb.org/3/search";
  private static final String BASE_URL_SERIES = "https://api.themoviedb.org/3/tv";

  private static RestTemplateBuilder requestBuilder = new RestTemplateBuilder();

  @Value("${api.provider.tmdb.key}")
  private String apiKey;

  public Movie getMovieDetailsById(long id) {

    logger.info("Making request to '{}'", BASE_URL_MOVIE);
    // Instead of getting from a DB, just make the request
    URI movie_url =
        UriComponentsBuilder.fromHttpUrl(BASE_URL_MOVIE)
            .path("/{id}")
            .queryParam("api_key", apiKey)
            .queryParam("language", getLocale())
            .buildAndExpand(id)
            .toUri();

    try {
      var resp = requestBuilder.build().getForObject(movie_url, Movie.class);

      List<Video> vids = getVideosForMovieId(id);
      // since we got the videos, attach that to the movie obj
      resp.setVideos(vids);
      return resp;
    } catch (HttpClientErrorException e) {
      logger.info("ERROR: {}", e.getRawStatusCode());
      return null;
    } catch (NullPointerException e) {
      logger.info("ERROR: {}", e.getMessage());
      return null;
    }
  }



  public List<Video> getVideosForMovieId(long id) {
    // after getting the movie successfully, also get the videos
    URI videos_url =
        UriComponentsBuilder.fromHttpUrl(BASE_URL_MOVIE)
            .path("/{id}/videos")
            .queryParam("api_key", apiKey)
            .queryParam("language", getLocale())
            .buildAndExpand(id)
            .toUri();
    VideoResults vids = requestBuilder.build().getForObject(videos_url, VideoResults.class);
    if (vids == null) {
      throw new NullPointerException();
    }
    return vids.getResults();
  }

  @Override
  public Series getSeriesDetailsById(long id) {
    URI series_url =
        UriComponentsBuilder.fromHttpUrl(BASE_URL_SERIES)
            .path("/{id}")
            .queryParam("api_key", apiKey)
            .queryParam("language", getLocale())
            .buildAndExpand(id)
            .toUri();

    try {
      var resp = requestBuilder.build().getForObject(series_url, Series.class);

      List<Video> vids = getVideosForSeriesId(id);
      // since we got the videos, attach that to the movie obj
      resp.setVideos(vids);
      return resp;
    } catch (HttpClientErrorException e) {
      logger.info("ERROR: {}", e.getRawStatusCode());
      return null;
    } catch (NullPointerException e) {
      logger.info("ERROR: {}", e.getMessage());
      return null;
    }

  }

  public List<Video> getVideosForSeriesId(long id) {
    // after getting the movie successfully, also get the videos
    URI videos_url =
            UriComponentsBuilder.fromHttpUrl(BASE_URL_SERIES)
                                .path("/{id}/videos")
                                .queryParam("api_key", apiKey)
                                .queryParam("language", getLocale())
                                .buildAndExpand(id)
                                .toUri();
    VideoResults vids = requestBuilder.build().getForObject(videos_url, VideoResults.class);
    if (vids == null) {
      throw new NullPointerException();
    }
    return vids.getResults();
  }

  @Override
  public Page<Movie> searchMovie(String query, PageRequest page) {
    int pageNumber = page.getPageNumber();

    URI url =
        UriComponentsBuilder.fromHttpUrl(BASE_URL_SEARCH)
            .path("/movie")
            .queryParam("api_key", apiKey)
            .queryParam("language", getLocale())
            .queryParam("query", query)
            .queryParam("page", pageNumber)
            .build()
            .toUri();

    //
    var rep = requestBuilder.build().getForObject(url, String.class);

    return null;
  }

  @Override
  public Page<Series> searchSeries(String query, PageRequest page) {
    int pageNumber = page.getPageNumber();
    URI url =
        UriComponentsBuilder.fromHttpUrl(BASE_URL_SEARCH)
            .path("/series")
            .queryParam("api_key", apiKey)
            .queryParam("language", getLocale())
            .queryParam("query", query)
            .queryParam("page", pageNumber)
            .build()
            .toUri();

    //
    var rep = requestBuilder.build().getForObject(url, String.class);
    return null;
  }

  private Locale getLocale() {
    return LocaleContextHolder.getLocale();
  }
}
