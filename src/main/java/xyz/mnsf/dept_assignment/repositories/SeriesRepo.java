package xyz.mnsf.dept_assignment.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import xyz.mnsf.dept_assignment.models.Series;

public interface SeriesRepo {

    Series getSeriesDetailsById(long id);

    Page<Series> searchSeries(String query, PageRequest pag);
}
