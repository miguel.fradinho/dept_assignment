package xyz.mnsf.dept_assignment.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import xyz.mnsf.dept_assignment.models.Movie;

public interface MovieRepo {

    Movie getMovieDetailsById(long id);

    Page<Movie> searchMovie(String query, PageRequest page);
}
