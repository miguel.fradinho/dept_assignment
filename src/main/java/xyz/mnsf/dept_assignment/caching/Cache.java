package xyz.mnsf.dept_assignment.caching;

import java.time.Instant;
import java.util.Set;

public interface Cache<T> {
  /**
   * Gets an element from the cache, if it exists
   *
   * @param key
   * @return element
   * @throws ElementNotFound if element does not exists in the cache
   */
  T get(String key) throws ElementNotFound;

  /**
   * Checks if the cache contains an element with the key provided
   *
   * @param key
   * @return true if the cache contains an element for the key provided
   */
  boolean containsKey(String key);

  /**
   * Inserts the provided element in the cache, expiring after an <b>implementation-defined</b>
   * interval.
   *
   * @param element element to cache
   * @see Cache#put(String, Object, Instant)
   * @see Cache#put(String, Object, long)
   */
  void put(String key, T element);

  /**
   * Inserts the provided element in the cache, expiring at {@code liveUntil}.
   *
   * @param element element to cache
   * @see Cache#put(String, Object, Instant)
   * @see Cache#put(String, Object, long)
   */
  void put(String key, T element, Instant liveUntil);

  /**
   * Inserts the provided element in the cache, expiring after the provided {@code ttl}.
   *
   * @param element element to cache
   * @see Cache#put(String, Object)
   * @see Cache#put(String, Object, Instant)
   */
  void put(String key, T element, long ttl);

  /** Clears the cache */
  void clear();

  /**
   * Gets the a set with keys of the cache
   *
   * @return the keys in the cache
   */
  Set<String> keySet();
}
