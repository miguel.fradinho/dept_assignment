package xyz.mnsf.dept_assignment.caching;

public class ElementNotFound extends Exception {
  public ElementNotFound() {
    super();
  }

  public ElementNotFound(String message) {
    super(message);
  }
}
