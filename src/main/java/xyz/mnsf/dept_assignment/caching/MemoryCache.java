package xyz.mnsf.dept_assignment.caching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import xyz.mnsf.dept_assignment.utils.Tuple;
import xyz.mnsf.dept_assignment.utils.Utils;

import javax.annotation.PreDestroy;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MemoryCache<T> extends Thread implements Cache<T> {
  public static final long DEFAULT_TTL_MILI = 10_000_000;
  public static final int RUN_EVERY_MILIS = 60 * 1000;
  public static final double DEFAULT_THRESHOLD = 0.25;
  private static Logger logger = LoggerFactory.getLogger(MemoryCache.class);
  private Map<String, Instant> timeMap = new ConcurrentHashMap<>();
  private Map<String, T> valueMap = new ConcurrentHashMap<>();
  private Instant lastPrune;
  /** Results of the last prune, KeysDeleted + Repeats done */
  private Tuple<Integer, Integer> lastPruneResults;

  private Map<Instant, Tuple<Integer, Integer>> prunesDone;
  private double repeatThreshold;

  public MemoryCache() {
    this.repeatThreshold = DEFAULT_THRESHOLD;
  }

  public MemoryCache(double threshold) {
    if (threshold > 0.0 && threshold < 1.0) {
      this.repeatThreshold = threshold;
      }
    else{
      throw new IllegalArgumentException("Invalid threshold values!");
    }
  }

  @PreDestroy
  public void cleanup() {
    this.interrupt();
  }

  public Map<Instant, Tuple<Integer, Integer>> getPrunesDone() {
    return prunesDone;
  }

  public Instant getLastPrune() {
    return lastPrune;
  }

  public Tuple<Integer, Integer> getLastPruneResults() {
    return lastPruneResults;
  }

  @Override
  public T get(String key) throws ElementNotFound {
    // Get the element from the map
    T val = valueMap.getOrDefault(key, null);
    // If there is one, check it's expire date
    if (val != null) {
      Instant expireTime = timeMap.get(key);
      Instant now = Instant.now();
      // If it already expired, delete it and throw an exception
      if (now.equals(expireTime) || (now.isAfter(expireTime))) {
        valueMap.remove(key);
        timeMap.remove(key);
        throw new ElementNotFound(key + " isn't present in the cache!");
      }
      return val;
    }
    // If there isn't any, just throw an exception
    throw new ElementNotFound(key + " isn't present in the cache!");
  }

  @Override
  public boolean containsKey(String key) {
    return valueMap.containsKey(key);
  }

  @Override
  public void put(String key, T element) {
    put(key, element, DEFAULT_TTL_MILI);
  }

  @Override
  public void put(String key, T element, Instant liveUntil) {
    timeMap.put(key, liveUntil);
    valueMap.put(key, element);
  }

  @Override
  public void put(String key, T element, long ttl) {
    Instant expireTime = Instant.now().plusMillis(ttl);
    put(key, element, expireTime);
  }

  @Override
  public void clear() {
    timeMap.clear();
    valueMap.clear();
  }

  @Override
  public Set<String> keySet() {
    return valueMap.keySet();
  }

  /**
   * Pruning function, Inspired by https://redis.io/commands/expire
   *
   * @param currentTime the time we're pruning
   * @return the number of keys deleted
   */
  public int prune(Instant currentTime) {
    // Get the keys currently in the map
    Set<String> keys = timeMap.keySet();
    // In order to get random keys, turn it into a list and shuffle
    List<String> shuffledKeys = new ArrayList<>(keys);
    // shuffle
    Collections.shuffle(shuffledKeys);
    // Get only the first 20 keys
    shuffledKeys = Utils.takeFirstNElements(shuffledKeys, 20);
    // For registering how many keys were deleted
    int keysDeleted = 0;

    for (String key : shuffledKeys) {
      Instant expireTime = timeMap.get(key);
      // If it already expires, delete it and increment the counter
      if (currentTime.equals(expireTime) || (currentTime.isAfter(expireTime))) {
        valueMap.remove(key);
        timeMap.remove(key);
        keysDeleted += 1;
      }
    }
    return keysDeleted;
  }

  @Override
  public void run() {
    logger.info("Initializing Cleaner Thread");
    while (true) {
      if (Thread.interrupted()) {
        logger.error("Cleaner Thread interrupted, stopping...");
        return;
      }
      Instant currentTime = Instant.now();
      logger.debug("Starting to prune at <{}>", currentTime);
      Tuple<Integer, Integer> totalPruneResults = Tuple.create(0, 0);
      int keysDeleted = 0;
      int currentSize;
      do {
        keysDeleted = prune(currentTime);
        currentSize = keySet().size();
        totalPruneResults =
            Tuple.create(
                totalPruneResults.getFirst() + keysDeleted, totalPruneResults.getSecond() + 1);
      } while (keysDeleted > (repeatThreshold * currentSize));

      logger.debug("Prune finished at <{}>", this.lastPrune);
      this.lastPrune = Instant.now();
      logger.debug("Saving results {}", totalPruneResults);
      this.lastPruneResults = totalPruneResults;
      prunesDone.put(this.lastPrune, this.lastPruneResults);
      try {
        Thread.sleep(RUN_EVERY_MILIS);
      } catch (InterruptedException e) {
        logger.error("Cleaner Thread Interrupted...", e);
        Thread.currentThread().interrupt();
        return;
      }
    }
  }
}
