package xyz.mnsf.dept_assignment.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import xyz.mnsf.dept_assignment.caching.Cache;
import xyz.mnsf.dept_assignment.caching.ElementNotFound;
import xyz.mnsf.dept_assignment.models.Series;
import xyz.mnsf.dept_assignment.repositories.SeriesRepo;

@Service
public class SeriesService extends BaseCachedService {
  private static final Logger logger = LoggerFactory.getLogger(SeriesService.class);

  private SeriesRepo seriesRepo;

  public SeriesService(@Autowired SeriesRepo SeriesRepo, @Autowired Cache<Object> cache) {
    super(cache);
    this.seriesRepo = SeriesRepo;
    this.CACHE_KEY_PREFIX = "series:";
  }

  public Series getSeriesDetails(Long seriesId) {
    String cacheKey = "id:" + seriesId;
    logger.info("Getting Series with id '{}' ", seriesId);
    try {
      Series s = getFromCache(cacheKey);
      logger.info("Found Series with id '{}' in cache", seriesId);
      return s;
    } catch (ElementNotFound e) {
      logger.debug("Getting Series '{}' from repo", seriesId);
      var s = seriesRepo.getSeriesDetailsById(seriesId);
      // store in cache.
      // It's okay even if it's null, because that way we avoid having to make
      // unnecessary requests to a not found before cache expires
      storeInCache(cacheKey, s);
      return s;
    }
  }

  public Page<Series> searchSeries(String query, PageRequest pag) {

    seriesRepo.searchSeries(query, pag);
    return null;
  }
}
