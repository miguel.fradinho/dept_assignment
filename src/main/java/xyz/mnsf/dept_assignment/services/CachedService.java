package xyz.mnsf.dept_assignment.services;

import xyz.mnsf.dept_assignment.caching.ElementNotFound;

import java.util.Map;

public interface CachedService {
  Map<String, Object> getStats();

  public <T> T getFromCache(String key) throws ElementNotFound;

  void storeInCache(String key, Object value);

  String getCachePrefix();
}
