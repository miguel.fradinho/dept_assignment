package xyz.mnsf.dept_assignment.services;

import xyz.mnsf.dept_assignment.caching.Cache;
import xyz.mnsf.dept_assignment.caching.ElementNotFound;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BaseCachedService implements CachedService {
  protected Cache<Object> cache;

  protected String CACHE_KEY_PREFIX = "";

  protected AtomicInteger totalRequests = new AtomicInteger(0);
  protected AtomicInteger cacheMisses = new AtomicInteger(0);
  protected AtomicInteger cacheHits = new AtomicInteger(0);

  protected BaseCachedService(Cache<Object> cache) {
    this.cache = cache;
  }

  @Override
  public final Map<String, Object> getStats() {
    Map<String, Object> stats = new HashMap<>();
    stats.put("totalRequests", totalRequests.intValue());
    stats.put("cacheHits", cacheHits.intValue());
    stats.put("cacheMisses", cacheMisses.intValue());
    return stats;
  }

  @Override
  public final <T> T getFromCache(String key) throws ElementNotFound {
    try {
      T val = (T) this.cache.get(CACHE_KEY_PREFIX + key);
      cacheHits.getAndIncrement();
      return val;
    } catch (ElementNotFound e) {
      cacheMisses.getAndIncrement();
      throw e;
    }
  }

  @Override
  public final void storeInCache(String key, Object value) {
    this.cache.put(CACHE_KEY_PREFIX + key, value);
  }

  @Override
  public final String getCachePrefix() {
    return CACHE_KEY_PREFIX;
  }
}
