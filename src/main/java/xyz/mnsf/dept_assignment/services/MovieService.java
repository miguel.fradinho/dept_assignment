package xyz.mnsf.dept_assignment.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import xyz.mnsf.dept_assignment.caching.Cache;
import xyz.mnsf.dept_assignment.caching.ElementNotFound;
import xyz.mnsf.dept_assignment.models.Movie;
import xyz.mnsf.dept_assignment.repositories.MovieRepo;

@Service
public class MovieService extends BaseCachedService {
  private static final Logger logger = LoggerFactory.getLogger(MovieService.class);

  private MovieRepo movieRepo;

  public MovieService(@Autowired MovieRepo movieRepo, @Autowired Cache<Object> cache) {
    super(cache);
    this.movieRepo = movieRepo;
  }

  public Movie getMovieDetails(Long movieId) {

    String cacheKey = "id:" + movieId;
    logger.info("Getting Movie with id '{}' ", movieId);
    try {
      Movie m = getFromCache(cacheKey);
      logger.info("Found Movie with id '{}' in cache", movieId);
      return m;
    } catch (ElementNotFound e) {
      logger.debug("Getting movie '{}' from repo", movieId);
      var m = movieRepo.getMovieDetailsById(movieId);
      // store in cache.
      // It's okay even if it's null, because that way we avoid having to make
      // unnecessary requests to a not found before cache expires
      storeInCache(cacheKey, m);
      return m;
    }
  }


  public Page<Movie> searchMovie(String query, PageRequest page) {

    var resultsPage = movieRepo.searchMovie(query, page);
    return null;
  }
}
