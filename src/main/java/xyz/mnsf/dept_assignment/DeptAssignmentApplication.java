package xyz.mnsf.dept_assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class DeptAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeptAssignmentApplication.class, args);
    }

}
