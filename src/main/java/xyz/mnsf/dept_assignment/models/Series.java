package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class Series extends BaseShow {

  @JsonProperty private Long id;
  @JsonProperty private String name;

  @JsonProperty("original_name")
  private String originalName;

  @JsonProperty("created_by")
  private List<SeriesCreator> createdBy;

  @JsonProperty("first_air_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate firstAirDate;

  @JsonProperty("last_air_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate lastAirDate;

  @JsonProperty("episode_run_time")
  private List<Integer> episodeRunTime;

  @JsonProperty("in_production")
  private Boolean inProduction;

  @JsonProperty() private List<String> languages;

  @JsonProperty("number_of_episodes")
  private Integer numberEpisodes;

  @JsonProperty("number_of_seasons")
  private Integer numberSeasons;

  @JsonProperty("origin_country")
  private List<String> originCountry;

  @JsonProperty private String status;
  @JsonProperty private String type;

  @JsonProperty("video_list")
  private Optional<List<Video>> videos;

  public void setVideos(List<Video> videos) {
    this.videos = Optional.of(videos);
  }
}
