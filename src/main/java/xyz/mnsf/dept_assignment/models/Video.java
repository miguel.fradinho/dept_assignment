package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class Video {

  @JsonProperty private String id;

  @JsonProperty("iso_639_1")
  private String languageIso;

  @JsonProperty private String name;

  @JsonProperty("iso_3166_1")
  private String countryIso;

  @JsonProperty private String key;

  @JsonProperty private String site;

  @JsonProperty private String size;

  @JsonProperty private String type;

  @JsonProperty private boolean official;

  @JsonProperty("published_at")
  private Instant publishedAt;
}
