package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseDataWithDescription extends BaseDataObject {
  @JsonProperty private String overview;
}
