package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductionCountry {

  @JsonProperty("iso_3166_1")
  private String isoCode;

  @JsonProperty private String name;
}
