package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public abstract class BaseShow {

  @JsonProperty private Boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty private List<BaseDataObject> genres;

  @JsonProperty private String overview;


  @JsonProperty private String homepage;

  @JsonProperty private Double popularity;
  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("production_countries")
  private List<ProductionCountry> productionCountries;

  @JsonProperty("production_companies")
  private List<BaseProductionCompany> productionCompanies;

  @JsonProperty("spoken_languages")
  private List<Language> spokenLanguages;

  @JsonProperty private String tagline;

  @JsonProperty("vote_average")
  private Double voteAverage;

  @JsonProperty("vote_count")
  private Long voteCount;
}
