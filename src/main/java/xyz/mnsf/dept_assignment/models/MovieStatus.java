package xyz.mnsf.dept_assignment.models;

public enum MovieStatus {

    Rumored("Rumored"),
    Planned("Planned"),
    InProduction("In Production"),
    PostProduction("Post Production"),
    Released("Released"),
    Canceled("Canceled");


    private String serializedValue;

    MovieStatus(String s) {
        this.serializedValue = s;
    }
}
