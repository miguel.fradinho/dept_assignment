package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;

public class MovieSearchResult {
  @JsonProperty private Boolean adult;

  @JsonProperty("backdrop_path")
  private String backdropPath;

  @JsonProperty("genre_ids")
  private List<Integer> genres;

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("original_title")
  private String originalTitle;

  @JsonProperty("original_language")
  private String originalLanguage;

  @JsonProperty private String overview;

  @JsonProperty private Double popularity;

  @JsonProperty("poster_path")
  private String posterPath;

  @JsonProperty("release_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate releaseDate;

  @JsonProperty private String title;

  @JsonProperty private Boolean video;

  @JsonProperty("vote_average")
  private Double voteAverage;

  @JsonProperty("vote_count")
  private Long voteCount;
}
