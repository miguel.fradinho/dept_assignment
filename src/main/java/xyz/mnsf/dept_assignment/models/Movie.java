package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class Movie extends BaseShow {

  @JsonProperty private Long id;

  @JsonProperty("belongs_to_collection")
  private Object belongsToCollection;

  @JsonProperty private Long budget;

  @JsonProperty("imdb_id")
  private String imdbId;

  @JsonProperty("original_title")
  private String originalTitle;

  @JsonProperty("release_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate releaseDate;

  @JsonProperty private Long revenue;
  @JsonProperty private Integer runtime;

  @JsonProperty private MovieStatus status;

  @JsonProperty private String title;

  @JsonProperty("video")
  private Boolean isVideo;

  @JsonProperty("video_list")
  private Optional<List<Video>> videos;

  public void setVideos(List<Video> videos) {
    this.videos = Optional.of(videos);
  }
}
