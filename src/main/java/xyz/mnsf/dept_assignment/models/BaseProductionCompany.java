package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseProductionCompany {
  @JsonProperty private Integer id;
  @JsonProperty private String name;

  @JsonProperty("logo_path")
  private String logoPath;

  @JsonProperty("origin_country")
  private String originCountry;
}
