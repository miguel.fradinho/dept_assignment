package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SeriesCreator {
  @JsonProperty private Long id;

  @JsonProperty("credit_id")
  private String creditId;

  @JsonProperty private String name;
  @JsonProperty private Integer gender;

  @JsonProperty("profile_path")
  private String profilePath;
}
