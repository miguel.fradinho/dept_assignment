package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Language {

  @JsonProperty("english_name")
  private String englishName;

  @JsonProperty("iso_639_1")
  private String isoCode;

  @JsonProperty private String name;
}
