package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class SearchResults<T> extends PageImpl<T> {

  public SearchResults(
      @JsonProperty("results") List<T> content,
      @JsonProperty("page") int page,
      @JsonProperty("total_pages") int size,
      @JsonProperty("total_results") Long totalElements) {
    super(content, PageRequest.of(page, size), totalElements);
  }
}
