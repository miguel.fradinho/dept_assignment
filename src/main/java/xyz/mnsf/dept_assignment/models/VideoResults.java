package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VideoResults {
  @JsonProperty("results")
  private List<Video> results;

  public List<Video> getResults() {
    return results;
  }
}
