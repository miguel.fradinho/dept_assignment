package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class Season extends BaseDataWithDescription {

  @JsonProperty("air_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  private LocalDate airDate;

  @JsonProperty("episode_count")
  private Integer episodeCount;

  @JsonProperty("season_number")
  private Integer seasonNumber;

  @JsonProperty("poster_path")
  private String posterPath;
}
