package xyz.mnsf.dept_assignment.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseDataObject {

  @JsonProperty private Integer id;
  @JsonProperty private String name;
}
