package xyz.mnsf.dept_assignment.utils;

import java.util.Objects;

public class Tuple<T, K> {
    private T x;
    private K y;

    private Tuple(T first, K second) {
        this.x = first;
        this.y = second;
    }

    public static <T, K> Tuple<T, K> create(T t, K k) {
        return new Tuple<>(t, k);
    }

    public T getFirst() {
        return x;
    }

    public K getSecond() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(x, tuple.x) &&
               Objects.equals(y, tuple.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
