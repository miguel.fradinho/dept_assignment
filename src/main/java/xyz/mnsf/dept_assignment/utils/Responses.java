package xyz.mnsf.dept_assignment.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public final class Responses {
  public static ResponseEntity.BodyBuilder baseJSONResponse(HttpStatus status) {
    return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON);
  }

  public static ResponseEntity errorResponseJSON(HttpStatus status, String msg) {
    return baseJSONResponse(status).body(ErrorResponse.build(status, msg));
  }

  public static ResponseEntity errorResponseJSON(HttpStatus status, Exception e) {
    return baseJSONResponse(status).body(ErrorResponse.build(status, e));
  }

  public static ResponseEntity badRequest(Exception e) {
    return baseJSONResponse(HttpStatus.BAD_REQUEST)
        .body(ErrorResponse.build(HttpStatus.BAD_REQUEST, e));
  }

  public static ResponseEntity badRequest() {
    return baseJSONResponse(HttpStatus.BAD_REQUEST)
        .body(ErrorResponse.build(HttpStatus.BAD_REQUEST, "Invalid values/path provided!"));
  }

  public static ResponseEntity ok() {
    return baseJSONResponse(HttpStatus.OK).build();
  }

  public static <T> ResponseEntity ok(T t) {
    return baseJSONResponse(HttpStatus.OK).body(t);
  }

  public static ResponseEntity serverError() {
    return baseJSONResponse(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(
            ErrorResponse.build(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "We apologize for the inconvenience, an error occurred"));
  }
}
