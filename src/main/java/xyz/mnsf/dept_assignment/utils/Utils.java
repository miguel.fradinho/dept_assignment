package xyz.mnsf.dept_assignment.utils;

import java.util.ArrayList;
import java.util.List;

public final class Utils {

  private Utils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Takes the first N elements of a list.
   *
   * @param list
   * @param n
   * @return either the list provided if <code>n >= list.size </code>, or a list with the first N
   *     elements from the list provided
   */
  public static <T> List<T> takeFirstNElements(List<T> list, int n) {
    if (list == null) {
      return new ArrayList<>();
    }
    if (n < 1) {
      throw new IllegalArgumentException("The number of elements to take must be at least 1!");
    }
    if (n >= list.size()) {
      return list;
    }
    return new ArrayList<>(list.subList(0, n));
  }


  public static <T> List<T> blankList() {
    return new ArrayList<>();
  }
}
