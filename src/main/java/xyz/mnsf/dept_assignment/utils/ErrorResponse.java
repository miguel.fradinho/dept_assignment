package xyz.mnsf.dept_assignment.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

public class ErrorResponse {
  private int code;

  @JsonProperty("msg")
  private String message;

  public ErrorResponse() {
    this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
    this.message = "No message provided";
  }

  public ErrorResponse(HttpStatus status) {
    this.code = status.value();
    this.message = "No message provided";
  }

  public ErrorResponse(HttpStatus status, Exception e) {
    this.code = status.value();
    this.message = e.getMessage();
  }

  public ErrorResponse(HttpStatus status, String msg) {
    this.code = status.value();
    this.message = msg;
  }

  public static ErrorResponse build(HttpStatus status, Exception e) {
    return new ErrorResponse(status, e);
  }

  public static ErrorResponse build(HttpStatus status, String msg) {
    return new ErrorResponse(status, msg);
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
