package xyz.mnsf.dept_assignment.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.mnsf.dept_assignment.models.Series;
import xyz.mnsf.dept_assignment.services.SeriesService;

import java.util.Locale;

@RestController
@RequestMapping("/api/series")
public class SeriesController {
  private static Logger logger = LoggerFactory.getLogger(SeriesController.class);

  private final SeriesService seriesService;

  SeriesController(@Autowired SeriesService seriesService) {
    this.seriesService = seriesService;
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<Series> getSeriesDetailsById(@PathVariable("id") Long seriesId) {

    Locale locale = LocaleContextHolder.getLocale();

    logger.info("Current Locale {}", locale);

    logger.info("GET /api/series/{}", seriesId);

    var series = this.seriesService.getSeriesDetails(seriesId);

    if (series == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(series);
  }
}
