package xyz.mnsf.dept_assignment.controllers;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.mnsf.dept_assignment.models.Movie;
import xyz.mnsf.dept_assignment.models.Series;
import xyz.mnsf.dept_assignment.services.MovieService;
import xyz.mnsf.dept_assignment.services.SeriesService;
import xyz.mnsf.dept_assignment.utils.Responses;

@RestController
@RequestMapping("/api/search")
public class SearchController {
  private static Logger logger = LoggerFactory.getLogger(SearchController.class);

  private final MovieService movieService;
  private final SeriesService seriesService;

  SearchController(@Autowired MovieService movieService, @Autowired SeriesService seriesService) {
    this.movieService = movieService;
    this.seriesService = seriesService;
  }

  @GetMapping(value = "/movie")
  public ResponseEntity searchMovies(
      @RequestParam("query") String query,
      @RequestParam(name = "page", defaultValue = "1") int page,
      @RequestParam(value = "total_pages", defaultValue = "1") int size) {
    if (page < 1 || page > size) {
      return Responses.badRequest();
    }

    // just some sane sanitizing
    String q = cleanQuery(query);

    PageRequest pag = PageRequest.of(page, size);
    Page<Movie> res = this.movieService.searchMovie(q, pag);
    return Responses.ok(res.getContent());
  }

  @GetMapping(value = "/series")
  public ResponseEntity searchSeries(
      @RequestParam("query") String query,
      @RequestParam(name = "page", defaultValue = "1") int page,
      @RequestParam(value = "total_pages", defaultValue = "1") int size) {
    if (page < 1 || page > size) {
      return Responses.badRequest();
    }

    // just some sane sanitizing
    String q = cleanQuery(query);

    PageRequest pag = PageRequest.of(page, size);

    Page<Series> res = this.seriesService.searchSeries(q, pag);
    return Responses.ok(res.getContent());
  }

  private static String cleanQuery(String query) {
    return StringEscapeUtils.escapeHtml4(query);
  }
}
