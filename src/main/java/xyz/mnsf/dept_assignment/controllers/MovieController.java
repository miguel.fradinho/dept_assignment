package xyz.mnsf.dept_assignment.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.mnsf.dept_assignment.services.MovieService;
import xyz.mnsf.dept_assignment.utils.Responses;

@RestController

@RequestMapping("/api/movie")
public class MovieController {
  private static Logger logger = LoggerFactory.getLogger(MovieController.class);

  private final MovieService movieService;

  MovieController(@Autowired MovieService movieService) {
    this.movieService = movieService;
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity getMovieDetailsById(@PathVariable("id") Long movieId) {
    logger.info("GET /api/movie/{}", movieId);

    var movie = this.movieService.getMovieDetails(movieId);

    // technically we could do exception handling here, but this just makes it easir rn
    if (movie == null) {
      return Responses.errorResponseJSON(HttpStatus.NOT_FOUND, "movie not found");
    }
    return Responses.ok(movie);
  }
}
