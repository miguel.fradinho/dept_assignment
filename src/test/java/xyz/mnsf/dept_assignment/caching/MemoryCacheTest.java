package xyz.mnsf.dept_assignment.caching;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class MemoryCacheTest {

  private MemoryCache<Integer> cache;
  private static final String FALSE_KEY = "potato wrong key";
  private static final String KEY = "cool key";
  private static final Integer VALUE = 123456;

  @BeforeEach
  public void setup() {
    this.cache = new MemoryCache<>();
  }

  @AfterEach
  public void cleanup() {
    this.cache.interrupt();
  }

  @Test
  public void containsKeyReturnsFalseWhenEmptyCache() {
    assertFalse(cache.containsKey(FALSE_KEY));
  }

  @Test
  public void getThrowsExceptionWhenEmptyCache() throws ElementNotFound {
    assertThrows(ElementNotFound.class, () -> cache.get(FALSE_KEY));
  }

  @Test
  public void getThrowsExceptionWhenKeyDoesntExist() throws ElementNotFound {
    cache.put(KEY, VALUE);
    assertThrows(ElementNotFound.class, () -> cache.get(FALSE_KEY));
  }

  @Test
  public void getReturnsValueBeforeItExpired() {
    cache.put(KEY, VALUE, MemoryCache.DEFAULT_TTL_MILI);

    assertDoesNotThrow(
        () -> {
          Integer val = cache.get(KEY);
          assertEquals(VALUE, val);
        });
  }

  @Test
  public void getThrowsExceptionAfterKeyExpired() throws ElementNotFound {
    cache.put(KEY, VALUE, Instant.now());

    assertThrows(ElementNotFound.class, () -> cache.get(KEY));
    cache.put(KEY, VALUE, Instant.now().minusSeconds(60));
    assertThrows(ElementNotFound.class, () -> cache.get(KEY));
  }

  @Test
  public void putInsertsElement() {
    cache.put(KEY, VALUE);
    assertTrue(cache.containsKey(KEY));
  }

  @Test
  public void clearRemovesKeys() {
    cache.put(KEY, VALUE);
    cache.clear();
    assertFalse(cache.containsKey(KEY));
  }
}
