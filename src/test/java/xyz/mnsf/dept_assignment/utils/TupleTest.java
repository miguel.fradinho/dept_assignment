package xyz.mnsf.dept_assignment.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(SpringExtension.class)
public class TupleTest {
  private static final Tuple<String, String> first = Tuple.create("A", "B");
  private static final Tuple<String, String> second = Tuple.create("A", "B");

  private static final Tuple<String, String> different = Tuple.create("B", "B");

  @Test
  public void tuplesAreEqualIfBothElementsAreEqual() {
    assertEquals(first, second);
    assertEquals(first.hashCode(), second.hashCode());
  }

  @Test
  public void tuplesAreDifferentIfAnyElementsAreDifferent() {
    assertNotEquals(first, different);
    assertNotEquals(second, different);
  }
}
