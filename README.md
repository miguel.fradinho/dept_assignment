# dept_assignment

Dept Assignment - Backend
miguel.fradinho


## Installation
Just in order to get the Static Code Analysis running locally, there's a docker-compose file that instantiates a SonarQube instance with Postgres as the database.
The username was for convenience, the email(s) were made up afaik, all the passwords in the `.env` files were randomly generated and were never used anywhere else before.




## Endpoints implemented
- All endpoints support a query parameter `language` for selecting the locale

- `/api/movie/<id>` - gets the details for a movie, with ID (as per TMDB) as the path param, returns a movie object inflated with the videos
- `/api/series/<id>` - gets the details for a TV show, with ID (as per TMDB) as the path param, returns a series object inflated with the videos for that
-  `/api/search/movie` - search for a movie, by the provided `query` [non functional]
- `/api/search/series` - search for a series, by the provided `query` [non functional]





## Considerations/Assumptions
1. The architecture was based on a classic servlet MVC spring-boot application.
2. The external API chosen was the one suggested, TheMovieDatabase [TMDB]
3. Since we don't have a database and instead have to get our data from an external API, our "Repositories" are really just mock for making the requests to TMDB, rather than making queries on the DB (if we were to only ever work with an external API, we could also forego the Repository layer and move the logic onto the service, it may make more sense depending on the specific use case)
4. Due to the previous point, where we place our "locale" logic is a bit tricky. If we followed a "pure" MVC logic, our Repositories should only ever handle data & data validation, and as such, "choosing which locale" to present should only be under Repository logic IF the underlying database is also storing the translations. Otherwise, it should be part of the Service-level logic. As a compromise (and it order to take advantage of TMDB's translations), we just take advantage and inject the locale directly to the repository to the Repository (through preferably, it should be stateless, delegating through arguments)
5. Regarding the Repo implementation, while technically we could extract each Repo to a different class, it's not particularly relevant here (would be from a purely theoretical encapsulating the TMDB endpoints)
6. Rather than taking an Exception-based approach when handling wrong responses/requests, we're returning null from Repo and upwards + using some utility classes that return a `ResponseEntity` instance just so it always provides a consistent JSON error message, without us having to create multiple exceptions (which would be the proper way to approach, but it'd take more time)
7. `MemoryCache` Serves as a similar implementation to a redis-like cache (without the hassle of configuring an instance)
8. Because of point 3, despite our "request" logic being on the Repository, caching is most often either at Controller or Service layers. Since Controller should only relate to HTTP requests, putting it at the service layer